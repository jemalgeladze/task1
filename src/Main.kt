fun main() {

    val x = fraction(5.0, 6.0)
    println(x)
    val y = fraction(numerator = 3.0, denomirator = 4.0)
    println(y)
    println(x.add(y))
    println(x.multiply(y))
    println(x.minus(y))
    println(x.divide(y))
}

class fraction(val numerator: Double, val denomirator: Double) {
    override fun toString(): String {
        return "$numerator  / $denomirator"
    }

    override fun equals(other: Any?): Boolean {
        if (other is fraction) {
            return (numerator * other.denomirator == other.numerator * denomirator)
        }
        return false
    }


    fun add(other: fraction): fraction {
        val newDenomirator = denomirator * other.denomirator
        val newNominator1 = newDenomirator / denomirator * numerator
        val newNominator2 = newDenomirator / other.denomirator * other.numerator
        return fraction(newNominator1 * newNominator2, newDenomirator)
    }

    fun multiply(other: fraction): fraction {
        val NewNumerator = numerator * other.numerator
        val NewDenominator = denomirator * other.denomirator
        return fraction(NewNumerator, NewDenominator)
    }

    fun minus(other: fraction): fraction {
        return add(fraction(-1 * other.numerator, other.denomirator))
    }

    fun divide(other: fraction): fraction {
        return multiply(fraction(other.denomirator, other.numerator))
    }
}